### SecOps workflow
Operational tasks are a pre-requisite before the Developer workflow can be completed. An Admin will perform the following tasks:
1. Create TFE workspace
2. Add workspace to Sentinel policy set
3. Set Cloud credentials variable
4. Create team and team token
5. Assign write access for team
6. Optional: setup webhook notification

You can use TFE Web UI to complete these tasks or the [tfe_admin.sh](scripts/tfe_admin.sh) script included here. 

#### Running the admin script
The script uses the Terraform Enterprise API to complete the above tasks. 
- Note: 
  - Currently steps 2 and 6 are not implemented. 
  - The tools `curl` and `jq` should be installed.

- `cd` into scripts directory: `cd scripts`
- In a terminal, export environment variables for Terraform Enterprise server, Workspace name, Organization and TFE Administrative token. The TFE token should be from a User in the Organization owners team.
```
export TFE_TOKEN="your-tfe-token"
export TFE_WORKSPACE="gitlab-gcp-demo"
export TFE_ORG="your-tfe-org"
export TFE_ADDR="app.terraform.io"
```
- Prepare the Google credentials service account json file
Reference: [set-variables-script](https://github.com/hashicorp/terraform-guides/tree/master/operations/variable-scripts#running-the-set-variablessh-script)
This script uses `curl` to set a `GOOGLE_CREDENTIALS` environment variable. The Google service account `.json` file must be pre-processed for the curl command to be successful. 
  - You can use the following substitution commands to do all that in vi:
```
:1,$s/\n//
:s/"/\\\\"/g
:s/\//\\\//g
:s/\\n/\\\\\\\\n/g
```
  - Alternatively, you can do the following global substitutions in Atom or another text editor:
    - Replace each newline with a blank. (In Atom, click the .* button in the Find control and then replace \n with a blank value. But then deselect the .* button before making the remaining substitutions.)
    - Replace each " with \\".
    - Replace each / with \/.
    - Replace each \n with \\\\n.

  - Save the path to transformed file in an environment variable:
```
export gcp_credentials="/path/to/transformed-gcp-credentials-file"
```
- Execute the script:
```
./tfe_admin.sh
```
If everything ran successfully, you should see an output representing a Team-Workspace object such as below:
```
Created team token
{"data":{"id":"tws-sx9pyudZwfPTj7sb","type":"team-workspaces","attributes":{"access":"write"},"relationships":{"team":{"data":{"id":"team-Ve29GmDaDYZaWYbV","type":"teams"},"links":{"related":"/api/v2/teams/gitlab-gcp-demo-1-team"}},"workspace":{"data":{"id":"ws-Za7sqxHRbvh8gMJD","type":"workspaces"},"links":{"related":"/api/v2/organizations/kawsar-org/workspaces/gitlab-gcp-demo-1"}}},"links":{"self":"/api/v2/team-workspaces/tws-sx9pyudZwfPTj7sb"}}}Created team access
Workspace ID is: gitlab-gcp-demo-1
TFE_TOKEN is: RCsiB9vXasdfgh.atlasv1.UgVJb3kKlG6zzmH2IBPDZwasdfgh9uAhCkC7kXwVMItZaXmXasdfghk5jy5GMUnS0AY
```

-  Provide workspace ID and team token to Developer

##### Cleanup:
Adjust `team_id` and `workspace_id` in [tfe_admin_cleanup.sh](scripts/tfe_admin_cleanup.sh) if needed.
```
cd scripts/
./tfe_admin_cleanup.sh
```
